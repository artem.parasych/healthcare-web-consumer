package com.rwhc.base;


import org.openqa.selenium.WebDriver;

import com.rwhc.pages.userDropdown.AccountSettingsPage;
import com.rwhc.pages.userDropdown.AppointmentsPage;
import com.rwhc.pages.userDropdown.InsuranceInformationPage;
import com.rwhc.pages.userDropdown.MyBenefitsPage;
import com.rwhc.pages.userDropdown.SavedDocFacPage;
import com.rwhc.pages.LogInPage;
import com.rwhc.pages.myCare.FindYourPrescriptionPage;
import com.rwhc.pages.topMenuTabs.ActivityPage;
import com.rwhc.pages.topMenuTabs.InboxPage;
import com.rwhc.pages.topMenuTabs.MyCarePage;


public class TopMenu {
	
	//TopMenu is not a Page, Pages have a TopMenu
	
	WebDriver driver;
	
	public TopMenu(WebDriver driver) {
		
		this.driver = driver;
		
	}

	public MyCarePage goToMyCare() {
		
		Page.click("myCareBtn_CSS");
		
		return new MyCarePage();

	}
	
	public ActivityPage goToActivity() {
		
		Page.click("activityBtn_CSS");
		
		return new ActivityPage();
		
	}
	
	public InboxPage goToInbox() {
		
		Page.click("inboxBtn_CSS");
		
		return new InboxPage();

	}
	
	public void goToUserDropdown() {
		
		Page.click("userDropdown_CSS");

	}
	
	public InsuranceInformationPage goToClaims() {
		
		goToUserDropdown();
		Page.click("claimsBtn_CSS");
		
		return new InsuranceInformationPage();

	}
	
	public MyBenefitsPage goToBenefits() {
		
		goToUserDropdown();
		Page.click("benefitsBtn_CSS");
		
		return new MyBenefitsPage();

	}
	
	public InsuranceInformationPage goToInsuranceCard() {
		
		goToUserDropdown();
		Page.click("insuranceCardBtn_CSS");
		
		return new InsuranceInformationPage();

	}
	
	public SavedDocFacPage goToSavedDoctorsFacilities() {
		
		goToUserDropdown();
		Page.click("savedDoctorsFacilitiesBtn_CSS");
		
		return new SavedDocFacPage();

	}
	
	public FindYourPrescriptionPage goToMyPrescription() {
		
		goToUserDropdown();
		Page.click("myPrescriptionBtn_CSS");
		
		return new FindYourPrescriptionPage();
		
	}
	
	
	public AppointmentsPage goToAppointments() {
		
		goToUserDropdown();
		Page.click("appointmentsBtn_CSS");
		
		return new AppointmentsPage();

	}
	
	public AccountSettingsPage goToAccountSettings() {
		
		goToUserDropdown();
		Page.click("accountSettingsBtn_CSS");
		
		return new AccountSettingsPage();

	}
	
	public LogInPage goToSignOut() {
		
		goToUserDropdown();
		Page.click("signOutBtn_CSS");
		
		return new LogInPage();

	}

}
