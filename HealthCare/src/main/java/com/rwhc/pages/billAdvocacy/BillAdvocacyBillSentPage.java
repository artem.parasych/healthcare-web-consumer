package com.rwhc.pages.billAdvocacy;

import com.rwhc.base.Page;
import com.rwhc.pages.myCare.BillAdvocacyPage;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class BillAdvocacyBillSentPage extends Page {
	
	public MyCarePage tapBack() {
		
		click("backBtnBABSP_CSS");
		
		return new MyCarePage();
		
	}
	
	public BillAdvocacyPage tapSendAnotherBill() {
		
		click("sendAnotherBillBtnBABSP_CSS");
		
		return new BillAdvocacyPage();
		
	}

}
