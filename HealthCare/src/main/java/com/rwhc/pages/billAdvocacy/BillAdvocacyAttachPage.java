package com.rwhc.pages.billAdvocacy;

import com.rwhc.base.Page;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class BillAdvocacyAttachPage extends Page {
	
	public MyCarePage tapBack() {
		
		click("backBtnBAAP_CSS");
		
		return new MyCarePage();
		
	}
	
	public void tapAddMorePhotos() {
		
		click("addMorePhotosBtn_CSS");
		
	}
	
	public BillAdvocacyBillSentPage tapSubmit() {
		
		click("submitBtnBAAP_CSS");
		
		return new BillAdvocacyBillSentPage();
		
	}
	
	public void tapDeleteFirstPhoto() {
		
		click("firstImageItemDeleteBtnBAAP_XPATH");
		
	}

}
