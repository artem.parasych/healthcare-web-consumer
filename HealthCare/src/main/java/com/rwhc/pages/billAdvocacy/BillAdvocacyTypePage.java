package com.rwhc.pages.billAdvocacy;

import com.rwhc.base.Page;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class BillAdvocacyTypePage extends Page {
	
	public void tapFirstType() {
		
		click("firstTypeBtnBATP_XPATH");

	}

	public MyCarePage tapBack() {
		
		click("backBtnBATP_CSS");
		
		return new MyCarePage();

	}
	
	public BillAdvocacyPaidPage tapNext() {
		
		click("nextBtnBATP_CSS");
		
		return new BillAdvocacyPaidPage();

	}


}
