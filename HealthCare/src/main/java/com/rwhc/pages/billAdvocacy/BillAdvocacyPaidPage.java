package com.rwhc.pages.billAdvocacy;

import com.rwhc.base.Page;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class BillAdvocacyPaidPage extends Page {
	
	public void tapFirstYesType() {
		
		click("firstYesTypeBtn_XPATH");
		
	}
	
	public MyCarePage tapBack() {
		
		click("backBtnBAPP_CSS");
		
		return new MyCarePage();
		
	}
	
	public BillAdvocacyAttachPage tapNext() {
		
		click("nextBtnBAPP_CSS");
		
		return new BillAdvocacyAttachPage();
		
	}

}
