package com.rwhc.pages;


import com.rwhc.base.Page;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class LogInPage extends Page{
	
	public MyCarePage processLogIn(String email, String password) {
		
		enterEmail(email);
		enterPassword(password);
		tapLogin();
		
		return new MyCarePage();
		
	}
	
	public void enterEmail(String email) {
		
		type("emailAddressString_CSS", email);
		
	}
	
	public void enterPassword(String password) {
		
		type("passwordString_CSS", password);
		
	}
	
	public ForgotPasswordPage goToForgotPassword() {

		click("forgotPasswordBtn_CSS");
		
		return new ForgotPasswordPage();

	}
	
	public SignUpPage goToSignUp() {
		
		click("signUpBtn_CSS");
		
		return new SignUpPage();
		
	}
	
	public void tapLogin() {
		
		click("logInBtn_CSS");
		
	}

}
