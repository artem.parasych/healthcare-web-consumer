package com.rwhc.pages.accountSettings;

import com.rwhc.base.Page;

public class ChangeEmailPage extends Page {
	
	public EmailHasBeenChangedPage processChangeEmail(String newEmail) {
		
		enterNewEmail(newEmail);
		tapSubmit();
		
		return new EmailHasBeenChangedPage();
		
	}
	
	public void enterNewEmail(String newEmail) {
		
		type("enterNewEmailStringCEP_CSS", newEmail);
		
	}
	
	public void tapBack() {
		
		click("backBenCEP_CSS");
		
	}
	
	public EmailHasBeenChangedPage tapSubmit() {
		
		click("submitBtnCEP_XPATH");
		
		return new EmailHasBeenChangedPage();
		
	}

}
