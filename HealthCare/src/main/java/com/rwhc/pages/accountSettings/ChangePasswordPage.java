package com.rwhc.pages.accountSettings;

import com.rwhc.base.Page;

public class ChangePasswordPage extends Page {
	
	public PasswordHasBeenChangedPage processChangePassword(String currentPassword, String newPassword, String passwordConfirmation) {
		
		enterCurrentPassword(currentPassword);
		enterNewPassword(newPassword);
		enterPasswordConfirmation(passwordConfirmation);
		tapSubmit();
		
		return new PasswordHasBeenChangedPage();
		
	}
	
	public void enterCurrentPassword(String currentPassword) {
		
		type("currentPasswordStringCPP_XPATH", currentPassword);
		
	}
	
	public void enterNewPassword(String newPassword) {
		
		type("newPasswordStringCPP_XPATH", newPassword);
		
	}
	
	public void enterPasswordConfirmation(String passwordConfirmation) {
		
		type("passwordConfirmationStringCPP_XPATH", passwordConfirmation);
		
	}
	
	public void tapBack() {
		
		click("backBtnCPP_CSS");
		
	}
	
	public PasswordHasBeenChangedPage tapSubmit() {
		
		click("submitBtnCPP_XPATH");
		
		return new PasswordHasBeenChangedPage();
		
	}

}
