package com.rwhc.pages;

import com.rwhc.base.Page;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class SignUpPage extends Page {
	
	public void processSignUpNoEmailChange(String lastName, String dateOfBirth, String socialSecurityNumber, 
			String password, String confirmPassword) throws InterruptedException {
		
		processFirstSignUpScreen(lastName, dateOfBirth, socialSecurityNumber);
		Thread.sleep(2000);
		processSecondSignUpScreenNoEmailChanges();
		processFourthSignUpScreen(password, confirmPassword);		
		
	}
	
	public void processFirstSignUpScreen(String lastName, String dateOfBirth, String socialSecurityNumber) {
		
		enterLastName(lastName);
		enterDateOfBirth(dateOfBirth);
		enterSocialSecurityNumber(socialSecurityNumber);
		tapContinue();
		
	}
	
	public void processSecondSignUpScreenNoEmailChanges() {
		
		waitTillVisible("continueBtnSUPS_CSS");
		tapContinueBtnSUPS();
		
	}
	
	public void processSecondSignUpScreenWithEmailChanges() {
		
		tapChangeBtn();
		
	}
	
	public void processThirdSignUpScreenWithEmailChanges(String email, String confirmEmail) {
		
		enterEmailString(email);
		enterConfirmEmailString(confirmEmail);
		tapContinueBtnSUPT();
		
	}
	
	public MyCarePage processFourthSignUpScreen(String password, String confirmPassword) {
		
		enterPassword(password);
		enterConfirmPassword(confirmPassword);
		tapTheCheckBox();
		tapGetStarted();
		
		return new MyCarePage();
		
	}
	
	//SignUp Page first
	public void enterLastName(String lastName) {
		
		type("lastNameStringSUP_CSS", lastName);
		
	}
	
	public void enterDateOfBirth(String dateOfBirth) {
		
		type("dateOfBirthStringSUP_CSS", dateOfBirth);
		
	}
	
	public void enterSocialSecurityNumber(String socialSecurityNumber) {
		
		clear("socialSecurityNumberStringSUP_CSS");
		type("socialSecurityNumberStringSUP_CSS", socialSecurityNumber);
		
	}
	
	public void tapContinue() {
		
		click("ContinueBtnSUP_CSS");
		
	}
	
	//SignUp Page second
	public void tapChangeBtn() {

		click("changeBtnSUPS_CSS");

	}
	
	public void tapContinueBtnSUPS() {
		
		click("continueBtnSUPS_CSS");

	}
	
	public void tapBackBtnSUPS() {

		click("backBtnSUPS_CSS");

	}
	
	//SignUp Page third
	public void enterEmailString(String email) {
		
		type("emailStringSUPT_CSS", email);
		
	}
	
	public void enterConfirmEmailString(String confirmEmail) {
		
		type("confirmEmailStringSUPT_CSS", confirmEmail);
		
	}
	
	public void tapContinueBtnSUPT() {

		click("continueBtnSUPT_CSS");

	}
	
	public void tapBackBtnSUPT() {

		click("backBtnSUPT_CSS");

	}
	
	//SignUp Page fourth	
	public void enterPassword(String password) {
		
		type("passwordStringSUPF_CSS", password);
		
	}
	
	public void enterConfirmPassword(String confirmPassword) {
		
		type("confirmPasswordStringSUPF_CSS", confirmPassword);
		
	}
	
	public void tapTheCheckBox() {
		
		click("termsAndConditionsCheckBoxSUPF_CSS");
		
	}
	
	
	public void tapGetStarted() {
		
		click("getStartedBtnSUPF_CSS");
		
	}
	
	public void tapBackBtnSUPF() {

		click("backBtnSUPF_CSS");

	}

}