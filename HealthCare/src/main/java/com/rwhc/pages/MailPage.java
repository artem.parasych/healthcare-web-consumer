package com.rwhc.pages;

import org.openqa.selenium.By;

import com.rwhc.base.Page;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class MailPage extends Page {
	
	public void processLogIn(String email, String password) {
		
		enterEmail(email);
		enterPassword(password);
		tapConfirmBtn();
		
	}
	
	public MyCarePage processVerification() {
		
		tapFirstEmail();
		tapConfirmRegistrationBtn();
		
		return new MyCarePage();
		
	}
	
	public PasswordResetPage processRestorePassword() {
		
		tapFirstEmail();
		tapResetPasswordBtn();
		
		return new PasswordResetPage();
		
	}

	public void enterEmail(String email) {
		
		type("emailStringMP_ID", email);

	}
	
	public void clearEmail() {
		
		driver.findElement(By.id(OR.getProperty("emailStringMP_ID"))).clear();
		
	}

	public void enterPassword(String password) {
		
		type("passwordStringMP_ID", password);

	}
	
	public void selectDomain(String domain) {
		
		select("selectDropdownMP_XPATH", domain);
		
	}

	public void tapConfirmBtn() {
		
		click("submitBtnMP_CSS");

	}
	
	public void tapFirstEmail() {
		
		click("firstEmailRecord_XPATH");
		
	}
	
	public void tapConfirmRegistrationBtn() {
		
		click("confirmRegistrationBtn_CSS");
		
	}
	
	public void tapResetPasswordBtn() {

		click("resetPasswordBtnEP_CSS");

	}

	public void tapExitBtn() {

		click("exitBtnEP_CSS");

	}

}
