package com.rwhc.pages;

import com.rwhc.base.Page;

public class ForgotPasswordPage extends Page{
	
	public void processResetPassword(String email) {
		
		enterEmail(email);
		tapResetPassword();
		
	}
	
	public void enterEmail(String email) {
		
		type("emailsString_CSS", email);
		
	}
	
	public void tapResetPassword() {
		
		click("resetPasswordBtn_CSS");
		
	}

}
