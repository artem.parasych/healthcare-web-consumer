package com.rwhc.pages;

import com.rwhc.base.Page;

public class PasswordResetPage extends Page {

	public void processPasswordReset(String newPassword, String confirmPassword) {
		
		enterNewPassword(newPassword);
		enterConfirmPassword(confirmPassword);
		tapSubmitBtn();
		
	}
	
	public void enterNewPassword(String newPassword) {
		
		type("enterPasswordStringEP_CSS", newPassword);

	}

	public void enterConfirmPassword(String confirmPassword) {
		
		type("enterConfirmPasswordStringEP_CSS", confirmPassword);

	}

	public void tapSubmitBtn() {
		
		click("submitBtnEP_CSS");

	}

}
