package com.rwhc.pages.topMenuTabs;

import com.rwhc.base.Page;
import com.rwhc.pages.myCare.NewCarePlanRequestPage;

public class ActivityPage extends Page {
	
	public NewCarePlanRequestPage tapCreateBtn() {
		
		click("createBtnRP_CSS");
		
		return new NewCarePlanRequestPage();
		
	}

}
