package com.rwhc.pages.topMenuTabs;

import com.rwhc.base.Page;
import com.rwhc.pages.findCare.FindCarePage;
import com.rwhc.pages.myCare.BillAdvocacyPage;
import com.rwhc.pages.myCare.EducationLibraryPage;
import com.rwhc.pages.myCare.FindYourPrescriptionPage;
import com.rwhc.pages.myCare.NewCarePlanRequestPage;

public class MyCarePage extends Page {

	public NewCarePlanRequestPage goToChatNow() {
		//
		click("chatNowBtn_CSS");
		
		return new NewCarePlanRequestPage();

	}

	public FindCarePage goToFindCare() {
		
		click("findCareBtn_CSS");
		
		return new FindCarePage();

	}

	public void goToTalkToADoctor() {
		
		click("talkToADoctorBtn_XPATH");

	}

	public EducationLibraryPage goToEducationLibrary() {
		
		click("educationLibraryBtn_XPATH");
		
		return new EducationLibraryPage();

	}

	public FindYourPrescriptionPage goToFindYourPrescription() {
		
		click("findYourPrescriptionBtn_XPATH");
		
		return new FindYourPrescriptionPage();

	}

	public BillAdvocacyPage goToBillAdvocacy() {
		
		click("billAdvocacyBtn_XPATH");
		
		return new BillAdvocacyPage();

	}

}
