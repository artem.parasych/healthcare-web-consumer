package com.rwhc.pages.educationLibrary;

import com.rwhc.base.Page;
import com.rwhc.pages.myCare.EducationLibraryPage;

public class EducationCategoryPage extends Page {
	
	public EducationArticlePage goToTheFirstArticle() {
		
		click("firstContentItemECL_XPATH");
		
		return new EducationArticlePage();
		
	}
	
	public EducationLibraryPage tapBack() {
		
		click("backBtnECL_CSS");
		
		return new EducationLibraryPage();
		
	}

}


/*public class EducationCategoryPage extends Page {
	
	private EducationLibraryPage parent = null;
	
	public EducationCategoryPage(EducationLibraryPage parent) {
		this.parent = parent;
	}
	public void goToTheFirstArticle() {
		
		click("firstContentItemECL_XPATH");
		
	}
	
	public EducationLibraryPage tapBack() {
		
		click("backBtnECL_CSS");
		
		return parent;
		
	}

}*/
