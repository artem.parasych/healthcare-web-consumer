package com.rwhc.pages.educationLibrary;

import com.rwhc.base.Page;

public class EducationArticlePage extends Page {
	
	public EducationCategoryPage tapBack() {
		
		click("backBtnAP_CSS");
		
		return new EducationCategoryPage();
		
	}

}
