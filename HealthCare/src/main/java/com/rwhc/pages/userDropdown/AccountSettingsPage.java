package com.rwhc.pages.userDropdown;

import com.rwhc.base.Page;
import com.rwhc.pages.accountSettings.ChangeEmailPage;
import com.rwhc.pages.accountSettings.ChangePasswordPage;

public class AccountSettingsPage extends Page {
	
	public ChangeEmailPage tapChangeEmail() {
		
		click("changeEmeilBtnASP_XPATH");
		
		return new ChangeEmailPage();
		
	}
	
	public ChangePasswordPage tapChangePassword() {
		
		click("changePasswordBtnASP_XPATH");
		
		return new ChangePasswordPage();
		
	}
	
	public void tapBack() {
		
		click("backBtnASP_CSS");
		
	}

}
