package com.rwhc.pages.userDropdown;

import com.rwhc.base.Page;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class AppointmentsPage extends Page {
	
	public MyCarePage tapBack() {
		
		click("backBtnAP_CSS");
		
		return new MyCarePage();
		
	}

}
