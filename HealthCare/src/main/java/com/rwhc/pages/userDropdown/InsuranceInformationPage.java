package com.rwhc.pages.userDropdown;

import com.rwhc.base.Page;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class InsuranceInformationPage extends Page {
	
	public MyCarePage tapCancel() {
		
		click("cancelBtnVIIP_CSS");
		
		return new MyCarePage();
		
	}

}
