package com.rwhc.pages.myCare;

import com.rwhc.base.Page;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class NewCarePlanRequestPage extends Page {
	
	public void enterSubject(String subject) {
		
		type("subjectStringRR_CSS", subject);

	}

	public void enterMessage(String Message) {
		
		type("messageStringRR_CSS", Message);

	}

	public MyCarePage tapBack() {
		
		click("backBtnRR_CSS");
		
		return new MyCarePage();

	}

	public MyCarePage tapSubmit() {
		
		click("submitActiveBtnRR_CSS");
		
		return new MyCarePage();

	}

}
