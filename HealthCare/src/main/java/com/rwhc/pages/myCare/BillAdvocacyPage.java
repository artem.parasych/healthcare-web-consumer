package com.rwhc.pages.myCare;

import com.rwhc.base.Page;
import com.rwhc.pages.billAdvocacy.BillAdvocacyTypePage;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class BillAdvocacyPage extends Page {
	
	public BillAdvocacyTypePage tapContinueBtn() {
		
		click("continueBtnBAP_CSS");
		
		return new BillAdvocacyTypePage();

	}

	public MyCarePage tapBackBtn() {
		
		click("backBtnBAP_CSS");
		
		return new MyCarePage();

	}

}
