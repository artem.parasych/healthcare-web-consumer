package com.rwhc.pages.myCare;

import com.rwhc.base.Page;
import com.rwhc.pages.educationLibrary.EducationArticlePage;
import com.rwhc.pages.educationLibrary.EducationCategoryPage;

public class EducationLibraryPage extends Page {

	public void enterSearchParam(String search) {

		type("searchStringELP_CSS", search);

	}

	public void tapSearch() {
		
		click("searchBtnELP_CSS");

	}
	
	public EducationArticlePage tapFirstSearchResultItem() {
		
		click("firstSearchResultItemELP_XPATH");
		
		return new EducationArticlePage();

	}

	public EducationCategoryPage tapFirstCategory() {
		
		click("firstEducationItem_XPATH");
		
		return new EducationCategoryPage();

	}
	
}
