package com.rwhc.pages.myCare;

import com.rwhc.base.Page;
import com.rwhc.pages.topMenuTabs.MyCarePage;

public class FindYourPrescriptionPage extends Page {
	
	public void enterSearchParam(String search) {
		
		type("searchStringFYPP_CSS", search);
		
	}
	
	public void enterZipCode(String zipCode) {
		
		type("zipCodeStringFYPP_CSS", zipCode);
		
	}
	
	public void tapResetLocation() {
		
		click("resetLocationBtnFYPP_CSS");
		
	}
	
	public void tapMapBtn() {
		
		click("mapBtnFYPP_CSS");
		
	}
	
	public MyCarePage tapCancelBtn() {
		
		click("cancelBtnFYPP_CSS");
		
		return new MyCarePage();
		
	}

}
