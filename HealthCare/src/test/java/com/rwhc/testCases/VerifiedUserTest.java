package com.rwhc.testCases;




import java.io.IOException;
import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.Page;
import com.rwhc.base.TopMenu;
import com.rwhc.pages.LogInPage;
import com.rwhc.utilities.Utilities;




public class VerifiedUserTest extends BaseTest {
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void verifiedUserTest(Hashtable<String,String>data) throws InterruptedException, IOException {
		
		//Log In
		LogInPage lp = new LogInPage();
		Page.waitTillVisible("emailAddressString_CSS");
		lp.processLogIn(data.get("email"), data.get("password"));
		Page.waitTillVisible("findCareBtn_CSS");
		//Verify access to the Insurance Card page
		TopMenu tp = new TopMenu(driver);
		tp.goToClaims();
		isElementPresent("pageTitleCP_CSS");
		verifyEquals(data.get("assertText"), getText("familyDeductibleBlockTitle_XPATH"));
		//Log out
		tp.goToSignOut();
		
		Thread.sleep(3000);
		
	}

}
