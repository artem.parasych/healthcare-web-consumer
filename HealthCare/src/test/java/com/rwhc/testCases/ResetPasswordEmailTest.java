package com.rwhc.testCases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.Page;
import com.rwhc.pages.MailPage;
import com.rwhc.pages.PasswordResetPage;
import com.rwhc.utilities.Utilities;

public class ResetPasswordEmailTest extends BaseTest {
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void resetEmailPasswordTest(Hashtable<String,String>data) throws InterruptedException, IOException {
		
		Page.driver.get("http://mail.ru");
		String oldTab = driver.getWindowHandle();
		
		MailPage mp = new MailPage();
		mp.clearEmail();
		mp.processLogIn(data.get("email"), data.get("password"));
		
		Page.waitTillVisible("firstEmailRecord_XPATH");
		
		PasswordResetPage prp = mp.processRestorePassword();
		Thread.sleep(2000);
		
		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		newTab.remove(oldTab);
		driver.switchTo().window(newTab.get(0));
		
		prp.processPasswordReset(data.get("newPassword"), data.get("confirmPassword"));
		Thread.sleep(2000);
		
		verifyEquals(getText("assertTextBlockRPT_XPATH"), data.get("assertText"));
		
		driver.close();
		driver.switchTo().window(oldTab);
		
		driver.get(config.getProperty("urlTest"));
		
		Thread.sleep(2000);
		
	}

}
