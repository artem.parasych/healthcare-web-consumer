package com.rwhc.testCases;

import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.Page;
import com.rwhc.pages.LogInPage;
import com.rwhc.utilities.Utilities;

public class NewPasswordLogInTest extends BaseTest {

	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void newPasswordLogInTest(Hashtable<String,String>data) {

		LogInPage lp = new LogInPage();
		lp.processLogIn(data.get("email"), data.get("newPassword"));
		Page.waitTillVisible("findCareBtn_CSS");

	}

}
