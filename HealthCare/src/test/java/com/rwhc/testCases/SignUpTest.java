package com.rwhc.testCases;

import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.Page;
import com.rwhc.base.TopMenu;
import com.rwhc.pages.LogInPage;
import com.rwhc.pages.SignUpPage;
import com.rwhc.utilities.Utilities;

public class SignUpTest extends BaseTest {
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void signUpTest(Hashtable<String, String> data) throws InterruptedException {
		
		//Sign Up
		LogInPage lip = new LogInPage();
		Page.waitTillVisible("emailAddressString_CSS");
		SignUpPage sup = lip.goToSignUp();
		sup.processSignUpNoEmailChange(data.get("lastName"), data.get("dateOfBirth"), data.get("socialSecurityNumber"),
				data.get("password"), data.get("confirmPassword"));
		Page.waitTillVisible("findCareBtn_CSS");
		//Log Out
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToSignOut();
		Thread.sleep(2000);
		
	}

}
