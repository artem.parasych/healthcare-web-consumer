package com.rwhc.testCases;

import java.io.IOException;
import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.pages.LogInPage;
import com.rwhc.pages.ForgotPasswordPage;
import com.rwhc.utilities.Utilities;

public class ResetPasswordTest extends BaseTest{
	
	@Test(dataProviderClass = Utilities.class, dataProvider = "dp")
	public void resetPasswordTest(Hashtable<String,String> data) throws InterruptedException, IOException {
		
		
		LogInPage lip = new LogInPage();
		ForgotPasswordPage rpp = lip.goToForgotPassword();
		rpp.processResetPassword(data.get("email"));
		
		verifyEquals(data.get("assertText"), getText("assertTextFPP_XPATH"));
		
		Thread.sleep(5000);
		
	}

}
