package com.rwhc.testCases;

import java.util.ArrayList;
import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.Page;
import com.rwhc.pages.MailPage;
import com.rwhc.utilities.Utilities;

public class EmailVerificationTest extends BaseTest {
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void emailVerificationTest(Hashtable<String,String>data) throws InterruptedException {
		
		Page.driver.get("http://mail.ru");
		Page.waitTillVisible("emailStringMP_ID");
		String oldTab = driver.getWindowHandle();
		
		MailPage mp = new MailPage();
		mp.processLogIn(data.get("email"), data.get("password"));
		
		Page.waitTillVisible("firstEmailRecord_XPATH");
		
		mp.processVerification();
		Thread.sleep(2000);
		
		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		newTab.remove(oldTab);
		driver.switchTo().window(newTab.get(0));
		driver.close();
		driver.switchTo().window(oldTab);
		
		Page.waitTillVisible("confirmRegistrationBtn_CSS");
		mp.tapExitBtn();
		
		driver.get(config.getProperty("urlTest"));
		
		Thread.sleep(2000);
		
	}
}
