package com.rwhc.testCases;

import java.io.IOException;
import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.Page;
import com.rwhc.base.TopMenu;
import com.rwhc.pages.LogInPage;
import com.rwhc.utilities.Utilities;

public class NotVerifiedUserTest extends BaseTest {
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void notVerifiedUserTest(Hashtable<String,String>data) throws IOException {
		
		LogInPage lip = new LogInPage();
		Page.waitTillVisible("emailAddressString_CSS");
		lip.processLogIn(data.get("email"), data.get("password"));
		Page.waitTillVisible("findCareBtn_CSS");
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToInsuranceCard();
		isElementPresent("notVerifiedUserMessageTitle_CSS");
		
		verifyEquals(data.get("assertText"), getText("notVerifiedUserMessageTitle_CSS"));
		
		tp.goToSignOut();
	}

}
