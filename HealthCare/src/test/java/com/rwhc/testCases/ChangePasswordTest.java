package com.rwhc.testCases;

import java.io.IOException;
import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.Page;
import com.rwhc.base.TopMenu;
import com.rwhc.pages.accountSettings.ChangePasswordPage;
//import com.rwhc.pages.accountSettings.PasswordHasBeenChangedPage;
import com.rwhc.pages.userDropdown.AccountSettingsPage;
import com.rwhc.utilities.Utilities;

public class ChangePasswordTest extends BaseTest {
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void changePasswordTest(Hashtable<String,String> data) throws IOException {
		
		TopMenu tp = new TopMenu(driver);
		AccountSettingsPage asp = tp.goToAccountSettings();
		ChangePasswordPage cpp = asp.tapChangePassword();
		/*PasswordHasBeenChangedPage phbc = */cpp.processChangePassword(data.get("currentPassword"), data.get("newPassword"), data.get("passwordConfirmation"));
		
		verifyEquals(data.get("assertText"), getText("successPopUpTextPHBUP_XPATH"));
		
		Page.driver.get(config.getProperty("urlTest"));
		
		//phbc.tapDone();
		
	}

}
