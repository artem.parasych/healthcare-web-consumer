package com.rwhc.testCases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.Page;
import com.rwhc.base.TopMenu;
import com.rwhc.pages.myCare.BillAdvocacyPage;
import com.rwhc.pages.myCare.NewCarePlanRequestPage;
import com.rwhc.pages.topMenuTabs.MyCarePage;
import com.rwhc.utilities.Utilities;

public class MyCareTransitionTest extends BaseTest{
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void myCareToChatNowTest(Hashtable<String,String>data) throws IOException, InterruptedException {
		
		MyCarePage dp = new MyCarePage();
		NewCarePlanRequestPage rrp = dp.goToChatNow();
		
		waitTillVisible("backBtnRR_CSS");
		
		isElementPresent("submitNotActiveBtnRR_CSS");
		verifyEquals(data.get("assertTextChatRequest"), getText("popUpTitle_CSS"));
		
		rrp.tapBack();
		
		waitTillVisible("findCareBtn_CSS");
		
	}
	
	@Test
	public void myCareFindCareTest() throws IOException, InterruptedException {
		
		MyCarePage dp = new MyCarePage();
		dp.goToFindCare();
		
		waitTillVisible("primaryCareBtnFCP_CSS");
		
		isElementPresent("urgentCareBtnFCP_CSS");
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToMyCare();
		
		
		waitTillVisible("chatNowBtn_CSS");
	
	}
	
	@Test
	public void myCareEducationLibraryTest() throws InterruptedException {
		
		MyCarePage dp = new MyCarePage();
		dp.goToEducationLibrary();
		
		waitTillVisible("searchBtnELP_CSS");
		
		isElementPresent("firstEducationItem_XPATH");
		isElementPresent("searchStringELP_CSS");
		
		TopMenu tp = new TopMenu(driver);
		tp.goToMyCare();
		
		waitTillVisible("chatNowBtn_CSS");
		
	}
	
	@Test
	public void dashboardFindYourPrescriptionTest() {
		
		MyCarePage dp = new MyCarePage();
		dp.goToFindYourPrescription();
		
		waitTillVisible("searchStringFYPP_CSS");
		
		TopMenu tp = new TopMenu(driver);
		tp.goToMyCare();

		waitTillVisible("chatNowBtn_CSS");
		
	}
	
	@Test
	public void myCareTalkToADoctorTest() throws InterruptedException {
		
		String oldTab = driver.getWindowHandle();
		
		MyCarePage dp = new MyCarePage();
		dp.goToTalkToADoctor();
		Thread.sleep(15000);
		
		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		newTab.remove(oldTab);
		driver.switchTo().window(newTab.get(0));
		Thread.sleep(3000);
		
		waitTillVisible("upcomingAppointmentBtnTTADP_CSS");
		
		driver.close();
		driver.switchTo().window(oldTab);
		
		waitTillVisible("chatNowBtn_CSS");
		
		
		
		Thread.sleep(2000);
		
	}
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void myCareBillAdvocacyTest(Hashtable<String,String>data) throws IOException, InterruptedException {
		
		MyCarePage dp = new MyCarePage();
		BillAdvocacyPage bap = dp.goToBillAdvocacy();
		
		waitTillVisible("backBtnBAP_CSS");
		
		isElementPresent("continueBtnBAP_CSS");
		verifyEquals(data.get("assertBillADvocacyTitle"), getText("assertTextBAP_CSS"));
		
		bap.tapBackBtn();
		
		waitTillVisible("chatNowBtn_CSS");
		
	}
	

}
