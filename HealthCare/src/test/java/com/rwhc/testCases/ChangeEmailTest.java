package com.rwhc.testCases;

import java.io.IOException;
import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.TopMenu;
import com.rwhc.pages.accountSettings.ChangeEmailPage;
import com.rwhc.pages.accountSettings.EmailHasBeenChangedPage;
import com.rwhc.pages.userDropdown.AccountSettingsPage;
import com.rwhc.utilities.Utilities;

public class ChangeEmailTest extends BaseTest {
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void changeEmailTest(Hashtable<String,String> data) throws IOException {
		
		TopMenu tp = new TopMenu(driver);
		AccountSettingsPage asp = tp.goToAccountSettings();
		ChangeEmailPage cep = asp.tapChangeEmail();
		EmailHasBeenChangedPage ehbcp = cep.processChangeEmail(data.get("newEmail"));
		
		waitTillVisible("doneBTNEHBUP_CSS");
		verifyEquals(data.get("assertText"), getText("successPopUpTextEHBUP_CSS"));
		
		ehbcp.tapDone();
		
	}

}
