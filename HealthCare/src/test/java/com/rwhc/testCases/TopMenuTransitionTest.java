package com.rwhc.testCases;

import java.io.IOException;
import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.TopMenu;
import com.rwhc.pages.myCare.NewCarePlanRequestPage;
import com.rwhc.pages.topMenuTabs.ActivityPage;
import com.rwhc.pages.topMenuTabs.MyCarePage;
import com.rwhc.pages.userDropdown.AppointmentsPage;
import com.rwhc.utilities.Utilities;

public class TopMenuTransitionTest extends BaseTest {
	
	@Test
	public void myCareTabTest() throws IOException {
		
		TopMenu tp = new TopMenu(driver);
		MyCarePage rp = tp.goToMyCare();
		
		waitTillVisible("findCareBtn_CSS");
		
		rp.goToFindCare();
		isElementPresent("urgentCareBtnFCP_CSS");
		tp.goToMyCare();
		
		waitTillVisible("chatNowBtn_CSS");
		
	}
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void activityTabTest(Hashtable<String,String>data) throws IOException {
		
		TopMenu tp = new TopMenu(driver);
		ActivityPage ap = tp.goToActivity();
		
		waitTillVisible("createBtnRP_CSS");
		
		NewCarePlanRequestPage ncprp = ap.tapCreateBtn();
		isElementPresent("subjectStringRR_CSS");
		ncprp.tapBack();
		
		waitTillVisible("roadmapPageTitle_CSS");
		
		verifyEquals(data.get("assertText"), getText("roadmapPageTitle_CSS"));
		
	}
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void inboxTabTest(Hashtable<String,String>data) throws IOException {
		
		TopMenu tp = new TopMenu(driver);
		tp.goToInbox();
		
		waitTillVisible("navigatorChatsPageTitle_XPATH");
		
		isElementPresent("navigatorChatsPageTitle_XPATH");
		verifyEquals(data.get("assertText"), getText("navigatorChatsPageTitle_XPATH"));
		
	}
	
	/*@Test
	public void claimsBtnTest() throws IOException {
		
		TopMenu tp = new TopMenu(driver);
		InsuranceInformationPage iip = tp.goToClaims();
		
		waitTillVisible("yearlySnapshotBlockTitle_XPATH");
		
		verifyEquals("Yearly Snapshot", getText("yearlySnapshotBlockTitle_XPATH"));
		verifyEquals("Plan Details", getText("planDetailsBlockTitle_XPATH"));
		verifyEquals("Insurance Information", getText("insuranceInformationBlockTitle_XPATH"));

		iip.tapCancel();

		waitTillVisible("findCareBtn_CSS");
		isElementPresent("talkToADoctorBtn_XPATH");

	}*/

	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void benefitsBtnTest(Hashtable<String,String>data) throws IOException {

		TopMenu tp = new TopMenu(driver);
		tp.goToBenefits();

		waitTillVisible("planTabMBP_CSS");
		verifyEquals(data.get("assertText"), getText("assertTextTitleMBP_XPATH"));
		isElementPresent("planTabMBP_CSS");
		isElementPresent("benefitsTabMBP_CSS");


	}

	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void insuranceCardBtnTest(Hashtable<String,String>data) throws IOException {

		TopMenu tp = new TopMenu(driver);
		tp.goToInsuranceCard();

		waitTillVisible("medicalTabVFICP_XPATH");
		isElementPresent("dentalTabVFICP_XPATH");
		verifyEquals(data.get("assertText"), getText("pagesTitleVICP_XPATH"));

	}

	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void savedDoctorsFacilitiesBtnTest(Hashtable<String,String>data) throws IOException {

		TopMenu tp = new TopMenu(driver);
		tp.goToSavedDoctorsFacilities();

		waitTillVisible("doctorsTabSDFP_CSS");
		isElementPresent("facilitiesTabSDFP_CSS");
		verifyEquals(data.get("assertText"), getText("pageTitleSDFP_XPATH"));

	}

		@Test
		public void myPrescriptionBtnTest() throws IOException {
			
			TopMenu tp = new TopMenu(driver);
			tp.goToMyPrescription();
			
			waitTillVisible("searchStringMPP_CSS");
			
		}
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void appointmentsBtnTest(Hashtable<String,String>data) throws IOException {
		
		TopMenu tp = new TopMenu(driver);
		AppointmentsPage ap = tp.goToAppointments();
		
		waitTillVisible("appointmentsTableTitle_CSS");
		
		verifyEquals(data.get("assertText"), getText("appointmentsTableTitle_CSS"));
		
		ap.tapBack();
		
		waitTillVisible("findCareBtn_CSS");
		isElementPresent("talkToADoctorBtn_XPATH");
		
	}
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void accountSettingsBtnTest(Hashtable<String,String>data) throws IOException {
		
		TopMenu tp = new TopMenu(driver);
		tp.goToAccountSettings();
		
		waitTillVisible("changeEmeilBtnASP_XPATH");
		
		verifyEquals(data.get("assertTextBlock1"), getText("accountInformationBlockTitle_XPATH"));
		verifyEquals(data.get("assertTextBlock2"), getText("employerInformationBlockTitle_XPATH"));
		
	}

}
