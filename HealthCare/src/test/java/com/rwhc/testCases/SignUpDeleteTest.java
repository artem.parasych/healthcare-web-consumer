package com.rwhc.testCases;

import java.io.IOException;
import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.rwhc.base.Page;
import com.rwhc.utilities.Utilities;

public class SignUpDeleteTest extends BaseTest {
	
	@Test(dataProviderClass = Utilities.class, dataProvider = "dp")
	public void signUpDeleteTest(Hashtable<String,String>data) throws InterruptedException, IOException {

		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\test\\resources\\executables\\AITAdmin.exe");
		

		Thread.sleep(2000);
		
		if (config.getProperty("urlTest").equals("http://consumer-staging.rightwayhealthcare.com")) {
			Page.driver.get("https://staging.rightwayhealthcare.com/admin/user");
		} else if (config.getProperty("urlTest").equals("http://consumer.rightwayhealthcare.com")) {
			Page.driver.get("https://api.rightwayhealthcare.com/admin/user");	
		}
	    		
	    
		Page.type("searchString_CSS", data.get("SearchText"));
		Page.driver.findElement(By.cssSelector("input[class='form-control input-small']")).sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		Page.click("deleteBtn_CSS");
		Thread.sleep(2000);
		
		String header = getText("headerAP_CSS");
		if(header.equals(data.get("Assert Text"))) {
			Page.click("confirmDeleteBtn_CSS");
		}else {
			Assert.fail("User is not found or valid to delete");
			Page.driver.quit();
		}	
		
	}

}
