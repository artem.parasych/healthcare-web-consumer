package com.rwhc.testCases;

import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.pages.LogInPage;
import com.rwhc.utilities.Utilities;

public class ChangedEmailPasswordLogInTest extends BaseTest {
	
	@Test(dataProviderClass=Utilities.class, dataProvider="dp")
	public void changedEmailPasswordLogInTest(Hashtable<String,String>data) {
		
		waitTillVisible("emailAddressString_CSS");
		
		LogInPage lip = new LogInPage();
		lip.processLogIn(data.get("email"), data.get("password"));
		
		waitTillVisible("findCareBtn_CSS");
		
	}

}
