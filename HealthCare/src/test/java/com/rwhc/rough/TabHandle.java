package com.rwhc.rough;

import java.util.ArrayList;

import org.openqa.selenium.By;

import com.rwhc.base.Page;

public class TabHandle extends Page {
	
	public void testTabs() {
	    driver.get("https://consumer-staging.rightwayhealthcare.com");

	    // considering that there is only one tab opened in that point.
	    String oldTab = driver.getWindowHandle();
	    

	    driver.findElement(By.linkText("Twitter Advertising Blog")).click();
	    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
	    newTab.remove(oldTab);
	    // change focus to new tab
	    driver.switchTo().window(newTab.get(0));

	    // Do what you want here, you are in the new tab

	    driver.close();
	    // change focus back to old tab
	    driver.switchTo().window(oldTab);

	    // Do what you want here, you are in the old tab
	}

}
