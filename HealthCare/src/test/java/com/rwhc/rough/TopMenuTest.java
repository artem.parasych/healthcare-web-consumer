package com.rwhc.rough;

import java.util.Hashtable;

import org.testng.annotations.Test;

import com.rwhc.base.Page;
import com.rwhc.pages.LogInPage;
import com.rwhc.testCases.BaseTest;
import com.rwhc.utilities.Utilities;

public class TopMenuTest extends BaseTest{
	
	@Test(dataProviderClass = Utilities.class, dataProvider = "dp")
	public void topMenuTest(Hashtable<String,String> data) throws InterruptedException {
		
		LogInPage lip = new LogInPage();
		lip.processLogIn(data.get("email"), data.get("password"));
		Thread.sleep(12000);
		Page.menu.goToSignOut();
		
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id<locator>));
		//wait.until(ExpectedConditions.elementToBeClickable(By.id<locator>));

	}

}
