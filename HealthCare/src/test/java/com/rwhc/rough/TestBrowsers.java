package com.rwhc.rough;


import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestBrowsers {

	public static void main(String[] args) throws InterruptedException {
	
	Logger log = Logger.getLogger("devpinoyLogger");
	
	//@Test
	//public void newOneTest() throws InterruptedException {
	
		System.setProperty("webdriver.gecko.driver", "D:\\QA Automation\\Downloads\\geckodriver-v0.21.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		log.debug("Firefox driver verified");
		//System.setProperty("webdriver.chrome.driver", "D:\\QA Automation\\Downloads\\chromedriver_win32\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		driver.get("https://consumer-staging.rightwayhealthcare.com/");
		Thread.sleep(3000);
		
		driver.quit();
	}

}
