package com.rwhc.rough;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class UrlChangeTest {

	public static void main(String[] args) throws InterruptedException, IOException {
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\executables\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		/*System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\executables\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();*/
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.get("http://consumer-staging.rightwayhealthcare.com");
		Thread.sleep(5000);
		//Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\test\\resources\\AIT\\AITAdmin.exe");
		Runtime.getRuntime().exec("D:\\QA Automation\\Downloads\\AutoIt\\AITAdmin.exe");
		Thread.sleep(5000);
		driver.get("https://staging.rightwayhealthcare.com/admin/user");
		Thread.sleep(15000);
		
		//Alert alert = driver.switchTo().alert();
		//alert.sendKeys("Adminlog");
		//alert.dismiss();
		//System.out.println(alert.getText());
		
		driver.quit();
		
		//Alert alert = driver.switchTo().alert();
		//alert.sendKeys("Adminlog");
		//alert.dismiss();
		//System.out.println(alert.getText());
		
	}

}
